$(document).ready(function () {
    $('.bar__btn').on('click', function () {
        $(this).toggleClass('active');
        if($(this).hasClass('active')){
            $('.header__bar').addClass('active');
        } else{
            $('.header__bar').removeClass('active');
        }
    });

    $(window).scroll(function () {
        if($(window).scrollTop() > 90){
            $('header').addClass('fixed')
        }else{
            $('header').removeClass('fixed')
        }
    });
});
