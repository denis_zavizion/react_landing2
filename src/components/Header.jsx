import React, { useEffect, useState } from 'react';

import logo from '../access/img/logo.svg';
import langicon from '../access/img/lang-icon.svg';
// import langiconua from '../access/img/flags/1x1/ua.svg';
import lngarrow from '../access/img/lng-arrow.svg';
import { getLanguage, getSlide } from './api/landing';

export default (props) => {
  const [status, setstatus] = useState(true);
  const [faq, setfaq] = useState();
  const [faqurl, setfaqurl] = useState();
  const [about, setabout] = useState();
  const [abouturl, setabouturl] = useState();

  const [login, setlogin] = useState();
  const [loginurl, setloginurl] = useState();
  const [create, setcreate] = useState();
  const [createurl, setcreateurl] = useState();
  const [language, setLanguage] = useState();

  const [menuClass, gamMenuClass] = useState('');
  async function gammenu() {
    if (menuClass == 'active') {
      gamMenuClass('');
      console.log('-----------');
    } else {
      await gamMenuClass('active');
      console.log('+++++++++++');
      console.log(menuClass);
    }
  }
  function lan(code) {
    props.setcode(code);
    setstatus(true);
  }
  async function start() {
    if (status == true) {
      const list = await getSlide(8, 8, props.code);
      setfaq(list['ThemeSlides'][0]['theme_slide_group_options'][0].option);
      setfaqurl(list['ThemeSlides'][0]['theme_slide_group_options'][0].value);
      setabout(list['ThemeSlides'][0]['theme_slide_group_options'][1].option);
      setabouturl(list['ThemeSlides'][0]['theme_slide_group_options'][1].value);
      const list2 = await getSlide(8, 9, props.code);
      setlogin(list2['ThemeSlides'][0]['theme_slide_group_options'][1].option);
      setloginurl(
        list2['ThemeSlides'][0]['theme_slide_group_options'][1].value,
      );
      setcreate(list2['ThemeSlides'][0]['theme_slide_group_options'][2].option);
      setcreateurl(
        list2['ThemeSlides'][0]['theme_slide_group_options'][2].value,
      );
      setstatus(false);
    }

    const langs = await getLanguage();
    const langsItems = langs.map((item, key) => (
      <li key={key}>
        <button
          className="languageCountry"
          onClick={() => {
            lan(item.code);
          }}
        >
          {item.code}
        </button>
      </li>
    ));
    setLanguage(langsItems);
  }

  useEffect(async () => {
    try {
      await start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);

  return (
    <header>
      <div className="container">
        <button type="button" className="bar__btn" onClick={gammenu}>
          <span></span>
          <span></span>
          <span></span>
        </button>
        <a className="logo__link" href="#/">
          <img className="logo" src={logo} alt="" />
        </a>
        <nav className="header__bar">
          <a className="header__link" href={faqurl}>
            {faq}
          </a>
          <a className="header__link" href={abouturl}>
            {about}
          </a>
          <div className="language">
            <div className="current">
              <img
                className="lng__arrow"
                src={`/flags/1x1/${props.code.toLowerCase()}.svg`}
                alt=""
              />
              {/*<img className="lng__arrow" src={langicon} alt="" />*/}
              <img src={lngarrow} alt="" />
            </div>
            <ul>{language}</ul>
          </div>
          <button
            className="auth__register"
            onClick={() => {
              document.location.replace(loginurl);
            }}
          >
            {login}
          </button>
          <button
            href={createurl}
            className="auth__register"
            onClick={() => {
              document.location.replace(createurl);
            }}
          >
            {create}
          </button>
        </nav>
      </div>
    </header>
  );
};
