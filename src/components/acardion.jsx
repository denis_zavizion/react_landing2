import React, {useState} from "react";

export default (props) => {
    const [isActive, setIsActive] = useState(false);
    return (
        <div className="accordion__item" onClick={() => setIsActive(!isActive)}>
            <div className={isActive ? 'accordion__title ccordion--active' : 'accordion__title'}>
                {props.title}
                <div className={isActive ? 'accordion__arrow arrow--rotate' : 'accordion__arrow'}>
                    <svg
                        width="12"
                        height="9"
                        viewBox="0 0 12 9"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M6 9L0.803849 -9.78799e-07L11.1962 -7.02746e-08L6 9Z"
                            fill="#B63F5F"
                        />
                    </svg>
                </div>
            </div>
            {
                isActive ? <div className="accordion__content" style={{display: "block"}}>
                    <p>
                        {props.description}
                    </p>
                </div> : null
            }
        </div>
  
    )
}