import React, { useEffect, useState } from 'react';
import logo from '../access/img/logo.svg';
import { getSlide } from './api/landing';

export default (props) => {
  const [Affiliates, setAffiliates] = useState();
  const [Sign, setSign] = useState();
  const [FAQ, setFAQ] = useState();
  const [General, setGeneral] = useState();
  const [Financial, setFinancial] = useState();
  const [Verification, setVerification] = useState();
  const [About, setAbout] = useState();
  const [Contacts, setContacts] = useState();
  const [PrivacyPolicy, setPrivacyPolicy] = useState();
  const [ServiceAgreement, setServiceAgreement] = useState();
  const [RiskDisclosure, setRiskDisclosure] = useState();
  const [RulesTradingOperations, setRulesTradingOperations] = useState();
  const [NonTrading, setNonTrading] = useState();
  const [RiskPaymentPolicy, setRiskPaymentPolicy] = useState();

  const [Affiliatesln, setAffiliateslln] = useState();
  const [Signln, setSignln] = useState();
  const [FAQln, setFAQln] = useState();
  const [Generalln, setGeneralln] = useState();
  const [Financialln, setFinancialln] = useState();
  const [Verificationln, setVerificationln] = useState();
  const [Aboutln, setAboutln] = useState();
  const [Contactsln, setContactsln] = useState();
  const [PrivacyPolicyln, setPrivacyPolicyln] = useState();
  const [ServiceAgreementln, setServiceAgreementln] = useState();
  const [RiskDisclosureln, setRiskDisclosureln] = useState();
  const [RulesTradingOperationsln, setRulesTradingOperationsln] = useState();
  const [NonTradingln, setNonTradingln] = useState();
  const [RiskPaymentPolicyln, setRiskPaymentPolicyln] = useState();

  async function start() {
    const list = await getSlide(8, 16, props.code);
    setAffiliates(
      list['ThemeSlides'][0]['theme_slide_group_options'][0].option,
    );
    setSign(list['ThemeSlides'][0]['theme_slide_group_options'][1].option);
    setFAQ(list['ThemeSlides'][0]['theme_slide_group_options'][2].option);
    setGeneral(list['ThemeSlides'][0]['theme_slide_group_options'][3].option);
    setFinancial(list['ThemeSlides'][0]['theme_slide_group_options'][4].option);
    setVerification(
      list['ThemeSlides'][0]['theme_slide_group_options'][5].option,
    );
    setAbout(list['ThemeSlides'][0]['theme_slide_group_options'][6].option);
    setContacts(list['ThemeSlides'][0]['theme_slide_group_options'][7].option);
    setPrivacyPolicy(
      list['ThemeSlides'][0]['theme_slide_group_options'][8].option,
    );
    setServiceAgreement(
      list['ThemeSlides'][0]['theme_slide_group_options'][9].option,
    );
    setRiskDisclosure(
      list['ThemeSlides'][0]['theme_slide_group_options'][10].option,
    );
    setRulesTradingOperations(
      list['ThemeSlides'][0]['theme_slide_group_options'][11].option,
    );
    setNonTrading(
      list['ThemeSlides'][0]['theme_slide_group_options'][12].option,
    );
    setRiskPaymentPolicy(
      list['ThemeSlides'][0]['theme_slide_group_options'][13].option,
    );

    // setAffiliatesln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][0].value,
    // );
    // setSignln(list['ThemeSlides'][0]['theme_slide_group_options'][1].value);
    // setFAQln(list['ThemeSlides'][0]['theme_slide_group_options'][2].value);
    // setGeneralln(list['ThemeSlides'][0]['theme_slide_group_options'][3].value);
    // setFinancialln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][4].value,
    // );
    // setVerificationln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][5].value,
    // );
    // setAboutln(list['ThemeSlides'][0]['theme_slide_group_options'][6].value);
    // setContactsln(list['ThemeSlides'][0]['theme_slide_group_options'][7].value);
    // setPrivacyPolicyln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][8].value,
    // );
    // setServiceAgreementln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][9].value,
    // );
    // setRiskDisclosureln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][10].value,
    // );
    // setRulesTradingOperationsln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][11].value,
    // );
    // setNonTradingln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][12].value,
    // );
    // setRiskPaymentPolicyln(
    //   list['ThemeSlides'][0]['theme_slide_group_options'][13].value,
    // );
  }

  useEffect(async () => {
    try {
      start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);
  return (
    <footer>
      <div className="container">
        <div className="foot__logo">
          <a href="/">
            <img src={logo} alt="" />
          </a>
        </div>
        <div className="foot__items">
          <div className="foot__item">
            <h3 className="foot__item__title">{Affiliates}</h3>
            <a className="foot__link" href="#/">
              {Sign}
            </a>
          </div>
          <div className="foot__item">
            <h3 className="foot__item__title">{FAQ}</h3>
            <a className="foot__link" href="#/">
              {General}
            </a>
            <a className="foot__link" href="#/">
              {Financial}
            </a>
            <a className="foot__link" href="#/">
              {Verification}
            </a>
          </div>
          <div className="foot__item">
            <h3 className="foot__item__title">{About}</h3>
            <a className="foot__link" href="#/">
              {Contacts}
            </a>
          </div>
          <div className="foot__item">
            <a className="foot__link" href="#/">
              {PrivacyPolicy}
            </a>
            <a className="foot__link" href="#/">
              {ServiceAgreement}
            </a>
            <a className="foot__link" href="#/">
              {RiskDisclosure}
            </a>
          </div>
          <div className="foot__item">
            <a className="foot__link" href="#/">
              {RulesTradingOperations}
            </a>
            <a className="foot__link" href="#/">
              {NonTrading}
            </a>
            <a className="foot__link" href="#/">
              {RiskPaymentPolicy}
            </a>
          </div>
        </div>
        <div className="bot__footer">
          <p>
            Copyright © 2021 <span> Quotex </span>. All rights reserved
          </p>
        </div>
      </div>
    </footer>
  );
};
