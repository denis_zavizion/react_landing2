import React, { useEffect, useState } from 'react';
import icon1 from '../../access/img/icon1.svg';
import icon2 from '../../access/img/icon2.svg';
import icon3 from '../../access/img/icon3.svg';
import icon4 from '../../access/img/icon4.svg';
import { getSlide } from '../api/landing';

export default (props) => {
  const [block1_title, setblock1_title] = useState();
  const [block2_title, setblock2_title] = useState();
  const [block3_title, setblock3_title] = useState();
  const [block4_title, setblock4_title] = useState();
  const [block1_description, setblock1_description] = useState();
  const [block2_description, setblock2_description] = useState();
  const [block3_description, setblock3_description] = useState();
  const [block4_description, setblock4_description] = useState();
  const [btnTryPlaying, setbtnTryPlaying] = useState();

  async function start() {
    const list = await getSlide(8, 11, props.code);
    setblock1_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][0].option,
    );
    setblock2_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][1].option,
    );
    setblock3_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][2].option,
    );
    setblock4_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][3].option,
    );
    setblock1_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][4].option,
    );
    setblock2_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][5].option,
    );
    setblock3_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][6].option,
    );
    setblock4_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][7].option,
    );
    setbtnTryPlaying(
      list['ThemeSlides'][0]['theme_slide_group_options'][8].option,
    );
  }

  useEffect(async () => {
    try {
      start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);
  return (
    <section className="quality">
      <div className="container">
        <div className="qual__contt">
          <div className="qual__item">
            <div className="qual__icon">
              <img src={icon1} alt="" />
            </div>
            <h3>{block1_title}</h3>
            <p>{block1_description}</p>
          </div>
          <div className="qual__item">
            <div className="qual__icon">
              <img src={icon2} alt="" />
            </div>
            <h3>{block2_title}</h3>
            <p>{block2_description}</p>
          </div>
          <div className="qual__item">
            <div className="qual__icon">
              <img src={icon3} alt="" />
            </div>
            <h3>{block3_title}</h3>
            <p>{block3_description}</p>
          </div>
          <div className="qual__item">
            <div className="qual__icon">
              <img src={icon4} alt="" />
            </div>
            <h3>{block4_title}</h3>
            <p>{block4_description}</p>
          </div>
        </div>
        <button type="button" className="ban__reg">
          {btnTryPlaying}
        </button>
      </div>
    </section>
  );
};
