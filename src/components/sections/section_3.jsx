import React, { useEffect, useState } from 'react';
import prognosis from '../../access/img//prognosis__bg.png';
import progarrow from '../../access/img/prog-arrow.svg';
import macicon from '../../access/img/mac-icon.png';
import { getSlide } from '../api/landing';

export default (props) => {
  const [title, setbtitle] = useState();
  const [description, setdescription] = useState();
  const [btn_red, setbtn_red] = useState();
  const [btn_green, setbtn_green] = useState();

  const [btn_redlink, setbtn_redlink] = useState();
  const [btn_greenlink, setbtn_greenlink] = useState();

  async function start() {
    const list = await getSlide(8, 11, props.code);
    setbtitle(list['ThemeSlides'][0]['theme_slide_group_options'][0].option);
    setdescription(
      list['ThemeSlides'][0]['theme_slide_group_options'][1].option,
    );
    setbtn_red(list['ThemeSlides'][0]['theme_slide_group_options'][2].option);
    setbtn_green(list['ThemeSlides'][0]['theme_slide_group_options'][3].option);
    setbtn_redlink(
      list['ThemeSlides'][0]['theme_slide_group_options'][2].value,
    );
    setbtn_greenlink(
      list['ThemeSlides'][0]['theme_slide_group_options'][3].value,
    );
  }

  useEffect(async () => {
    try {
      start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);
  return (
    <section className="prognosis">
      <div className="bg__image">
        <img src={prognosis} alt="" />
      </div>
      <div className="container">
        <div className="prognosis__desc">
          <h2 className="money__title">
            {title}
            {/*To make more money, make the correct prognosis <br />*/}
            {/*Will the price go Up or Down?*/}
          </h2>
          <div className="prognosis__btns">
            <p className="money__dsc">{description}</p>
            <div className="price__btn">
              <a href={btn_redlink} className="down__btn">
                {btn_red} <img src={progarrow} alt="" />
              </a>
              <a href={btn_greenlink} className="up__btn">
                {btn_green} <img src={progarrow} alt="" />
              </a>
            </div>
          </div>
        </div>
        <div className="big__comp">
          <img className="money__ban" src={macicon} alt="" />
        </div>
      </div>
    </section>
  );
};
