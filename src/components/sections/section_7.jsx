import React, { useEffect, useState } from 'react';
import trade_bg from '../../access/img/trade_bg.svg';
import arrow_reg from '../../access/img/arrow-reg.svg';
import { getSlide } from '../api/landing';

export default (props) => {
  const [title, setbtitle] = useState();
  const [red, setbtn_red] = useState();
  const [DemoAccount, setbtn_green] = useState();

  async function start() {
    const list = await getSlide(8, 15, props.code);
    setbtitle(list['ThemeSlides'][0]['theme_slide_group_options'][0].option);
    setbtn_red(list['ThemeSlides'][0]['theme_slide_group_options'][1].option);
    setbtn_green(list['ThemeSlides'][0]['theme_slide_group_options'][2].option);
  }

  useEffect(async () => {
    try {
      start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);
  return (
    <section className="foot__banner">
      <div className="bg__img">
        <img src={trade_bg} alt="" />
      </div>
      <div className="container">
        <div className="banner__content">
          <h2>{title}</h2>
          <div className="auth__btns">
            <button className="real__acc">
              {red} <img src={arrow_reg} alt="" />
            </button>
            <button className="demo__acc">{DemoAccount}</button>
          </div>
        </div>
      </div>
    </section>
  );
};
