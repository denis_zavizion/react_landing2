import React, { useEffect, useState } from 'react';
import stepicon1 from '../../access/img/step_icon1.svg';
import linkarrow from '../../access/img/link-arrow.svg';
import stepicon2 from '../../access/img/step_icon2.svg';
import stepicon3 from '../../access/img/step_icon3.svg';
import { getSlide } from '../api/landing';

export default (props) => {
  const [title, settitle] = useState();
  const [block1_title, setblock1_title] = useState();
  const [block2_title, setblock2_title] = useState();
  const [block3_title, setblock3_title] = useState();

  const [block1_description, setblock1_description] = useState();
  const [block2_description, setblock2_description] = useState();
  const [block3_description, setblock3_description] = useState();

  const [block1_short_description, setblock1_short_description] = useState();
  const [block2_short_description, setblock2_short_description] = useState();
  const [block3_short_description, setblock3_short_description] = useState();

  const [block1shortdescrlink, setblock1shortdescrlink] = useState();
  const [block2shortdescrlink, setblock2shortdescrlink] = useState();
  const [block3shortdescrlink, setblock3shortdescrlink] = useState();

  async function start() {
    const list = await getSlide(8, 13, props.code);
    settitle(list['ThemeSlides'][0]['theme_slide_group_options'][0].option);
    setblock1_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][1].option,
    );
    setblock2_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][2].option,
    );
    setblock3_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][3].option,
    );
    setblock1_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][4].option,
    );
    setblock2_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][5].option,
    );
    setblock3_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][6].option,
    );

    setblock1_short_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][7].option,
    );
    setblock2_short_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][8].option,
    );
    setblock3_short_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][9].option,
    );

    setblock1shortdescrlink(
      list['ThemeSlides'][0]['theme_slide_group_options'][7].value,
    );
    setblock2shortdescrlink(
      list['ThemeSlides'][0]['theme_slide_group_options'][8].value,
    );
    setblock3shortdescrlink(
      list['ThemeSlides'][0]['theme_slide_group_options'][9].value,
    );
  }

  useEffect(async () => {
    try {
      start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);
  return (
    <section className="start">
      <div className="container">
        <div className="trading__steps">
          <h2 className="start__title">{title}</h2>
          <div className="steps">
            <div className="steps__dsc">
              <div className="steps__item">
                <div className="step__order">1</div>
                <div className="step__icon">
                  <img src={stepicon1} alt="" />
                </div>
                <div className="step__desc">
                  <h3>{block1_title}</h3>
                  <p>{block1_description}</p>
                  <a href={block1shortdescrlink}>
                    {block1_short_description}
                    <img src={linkarrow} alt="" />
                  </a>
                </div>
              </div>
              <div className="steps__item">
                <div className="step__order">2</div>
                <div className="step__icon">
                  <img src={stepicon2} alt="" />
                </div>
                <div className="step__desc">
                  <h3>{block2_title}</h3>
                  <p>{block2_description}</p>
                  <a href={block2shortdescrlink}>
                    {block2_short_description}
                    <img src={linkarrow} alt="" />
                  </a>
                </div>
              </div>
              <div className="steps__item">
                <div className="step__order">3</div>
                <div className="step__icon">
                  <img src={stepicon3} alt="" />
                </div>
                <div className="step__desc">
                  <h3>{block3_title}</h3>
                  <p>{block3_description}</p>
                  <a href={block3shortdescrlink}>
                    {block3_short_description} <img src={linkarrow} alt="" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
