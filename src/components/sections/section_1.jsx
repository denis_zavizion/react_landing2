import React, { useEffect, useState } from 'react';
import arrow_reg from '../../access/img/arrow-reg.svg';
import banner from '../../access/img/banner.png';
import rocket201 from '../../access/img/002rocket1.svg';
import brain1 from '../../access/img/003brain1.svg';
import headphones1 from '../../access/img/001headphones1.svg';
import { getSlide } from '../api/landing';

export default (props) => {
  const [status, setstatus] = useState(true);
  const [title, settitle] = useState();
  const [CreateDemoAccount, setbtnCreateDemoAccount] = useState();
  const [Innovate_platform, setInnovate_platform] = useState();
  const [Freepractice, setFreepractice] = useState();
  const [Support, setSupport] = useState();

  function lan(code) {
    props.setcode(code);
    setstatus(true);
  }
  async function start() {
    const list = await getSlide(8, 10, props.code);
    settitle(list['ThemeSlides'][0]['theme_slide_group_options'][0].option);
    setbtnCreateDemoAccount(
      list['ThemeSlides'][0]['theme_slide_group_options'][1].option,
    );
    setInnovate_platform(
      list['ThemeSlides'][0]['theme_slide_group_options'][2].option,
    );
    setFreepractice(
      list['ThemeSlides'][0]['theme_slide_group_options'][3].option,
    );
    setSupport(list['ThemeSlides'][0]['theme_slide_group_options'][4].option);
  }

  useEffect(async () => {
    try {
      start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);

  return (
    <section className="banner">
      <div className="bg__image"></div>
      <div className="container">
        <div className="banner__dsc">
          <h2 className="banner__title">{title}</h2>
          <div className="ban__auth">
            <button className="ban__reg">
              Create Account <img src={arrow_reg} alt="" />
            </button>
            <button className="ban__regdemo">{CreateDemoAccount}</button>
          </div>
          <div className="check__bar">
            <div className="chck__item">
              <img src={rocket201} alt="" />
              <h3>{Innovate_platform}</h3>
            </div>
            <div className="chck__item">
              <img src={brain1} alt="" />
              <h3>{Freepractice}</h3>
            </div>
            <div className="chck__item">
              <img src={headphones1} alt="" />
              <h3>{Support}</h3>
            </div>
          </div>
        </div>
        <div className="banner__img">
          <img src={banner} alt="" />
        </div>
      </div>
    </section>
  );
};
