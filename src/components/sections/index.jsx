import React from 'react';
import Section_1 from './section_1';
import Section_2 from './section_2';
import Section_3 from './section_3';
import Section_4 from './section_4';
import Section_5 from './section_5';
import Section_6 from './section_6';
import Section_7 from './section_7';

export default (props) => {
  return (
    <div>
      <Section_1 code={props.code} setcode={props.setcode} />
      <Section_2 code={props.code} setcode={props.setcode} />
      <Section_3 code={props.code} setcode={props.setcode} />
      <Section_4 code={props.code} setcode={props.setcode} />
      <Section_5 code={props.code} setcode={props.setcode} />
      <Section_6 code={props.code} setcode={props.setcode} />
      <Section_7 code={props.code} setcode={props.setcode} />
    </div>
  );
};
