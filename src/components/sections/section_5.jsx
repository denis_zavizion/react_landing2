import React, { useEffect, useState } from 'react';
// import React, {useState} from 'react';
import payicon1 from '../../access/img/pay-icon-1.svg';
import payicon2 from '../../access/img/pay-icon-2.svg';
import payicon3 from '../../access/img/pay-icon-3.svg';
import payicon4 from '../../access/img/pay-icon-4.svg';
import payicon5 from '../../access/img/pay-icon-5.svg';
import payicon6 from '../../access/img/pay-icon-6.svg';
import payicon7 from '../../access/img/pay-icon-7.svg';

export default (props) => {
  return (
    <section className="pay">
      <img src={payicon1} alt="" />
      <img src={payicon2} alt="" />
      <img src={payicon3} alt="" />
      <img src={payicon4} alt="" />
      <img src={payicon5} alt="" />
      <img src={payicon6} alt="" />
      <img src={payicon7} alt="" />
    </section>
  );
};
