import React, { useEffect, useState } from 'react';
import { getSlide } from '../api/landing';

export default (props) => {
  const [title, settitle] = useState();
  const [block1_title, setblock1_title] = useState();
  const [block2_title, setblock2_title] = useState();
  const [block3_title, setblock3_title] = useState();
  const [block4_title, setblock4_title] = useState();

  const [block1_description, setblock1_description] = useState();
  const [block2_description, setblock2_description] = useState();
  const [block3_description, setblock3_description] = useState();
  const [block4_description, setblock4_description] = useState();

  const [btn, setbtn] = useState();

  async function start() {
    const list = await getSlide(8, 14, props.code);
    settitle(list['ThemeSlides'][0]['theme_slide_group_options'][0].option);
    setblock1_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][1].option,
    );
    setblock2_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][2].option,
    );
    setblock3_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][3].option,
    );
    setblock4_title(
      list['ThemeSlides'][0]['theme_slide_group_options'][4].option,
    );
    setblock1_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][5].option,
    );
    setblock2_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][6].option,
    );
    setblock3_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][7].option,
    );
    setblock4_description(
      list['ThemeSlides'][0]['theme_slide_group_options'][8].option,
    );
    setbtn(list['ThemeSlides'][0]['theme_slide_group_options'][9].option);
  }

  useEffect(async () => {
    try {
      start();
    } catch (error) {
      console.log(error);
    }
  }, [props.code, props.setcode]);
  return (
    <section className="question">
      <div className="container">
        <h2 className="ques__title">{title}</h2>
        <div className="question__cont">
          <div className="half">
            <div className="question__item">
              <div className="question__title">{block1_title}</div>
              <p>{block1_description}</p>
            </div>
            <div className="question__item">
              <div className="question__title">{block2_title}</div>
              <p>{block2_description}</p>
            </div>
          </div>
          <div className="half">
            <div className="question__item">
              <div className="question__title">{block3_title}</div>
              <p>{block3_description}</p>
            </div>
            <div className="question__item">
              <div className="question__title">{block4_title}</div>
              <p>{block4_description}</p>
            </div>
          </div>
          <button type="button" className="all__btn">
            {btn}
          </button>
        </div>
      </div>
    </section>
  );
};
