//dispatching in API

export const getLanguage = async () => {
  const location = `http://back.quotex.ai/api/getLanguage/all`;
  const result = await fetch(location).then((res) => res.json());
  return result;
};

export const getSlide = async (themeid, slideid, code) => {
  const location_slide =
    `http://back.quotex.ai/api/getLandingSlide/theme/` +
    themeid +
    `/slide/` +
    slideid +
    `/lang/`;

  const result = await fetch(location_slide + code).then((res) => res.json());
  return result;
};
