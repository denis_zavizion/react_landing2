import React from 'react';

import Sections from './sections/index';
import { Footer, Header } from './component';

export default (props) => {
  return (
    <div>
      <Header code={props.code} setcode={props.setcode} />
      <Sections code={props.code} setcode={props.setcode} />
      <Footer code={props.code} setcode={props.setcode} />
    </div>
  );
};
