import React, { useEffect, useState } from 'react';
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';

import './access/css/libs.css';
import './access/css/main.css';
import './access/css/media.css';

import { Content } from './components/component';
import axios from 'axios';

function App() {
  const [code, setcode] = useState('EN');
  const getGeoInfo = () => {
    axios
      .get('https://ipapi.co/json/')
      .then((response) => {
        let data = response.data;
        setcode(data.country_code);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(async () => {
    try {
      getGeoInfo();
    } catch (error) {
      console.log(error);
    }
  }, []);
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path={`/${code.toLowerCase()}`}>
            {/*<Route path="en">*/}
            <Content code={code} setcode={setcode} />
          </Route>
          {/*<Redirect to="en" />*/}
          <Redirect to={`/${code.toLowerCase()}`} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
